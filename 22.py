# Используйте names.txt, текстовый файл размером 46 КБ, содержащий более пяти тысяч имен.
# Начните с сортировки в алфавитном порядке. Затем подсчитайте алфавитные значения каждого имени
# и умножьте это значение на порядковый номер имени в отсортированном списке для получения количества очков имени.
#
# Например, если список отсортирован по алфавиту,
# имя COLIN (алфавитное значение которого 3 + 15 + 12 + 9 + 14 = 53) является 938-м в списке.
# Поэтому, имя COLIN получает 938 × 53 = 49714 очков.
#
# Какова сумма очков имен в файле?


import string


sum_of_char = 0
sum_of_scores = 0
counter = 0

with open("names.txt") as file:
    list_of_names = file.read().split('","')
    list_of_names[0] = list_of_names[0][1:]
    list_of_names[-1] = list_of_names[-1][:-1]
    list_of_names.sort()
    for name in list_of_names:
        sum_of_char = 0
        for char in name:
            sum_of_char += string.ascii_uppercase.index(char) + 1
        sum_of_scores += sum_of_char * (list_of_names.index(name) + 1)

print(sum_of_scores)
