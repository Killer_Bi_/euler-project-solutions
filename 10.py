# Сумма простых чисел меньше 10 равна 2 + 3 + 5 + 7 = 17.
#
# Найдите сумму всех простых чисел меньше двух миллионов.


def is_prime(n):
    if n % 2 == 0:
        return n == 2
    for i in range(3, int(n**(1/2)) + 1, 2):
        if n % i == 0:
            return False
    return True


sum_of_primes = 2
num = 3
while num < 2000000:
    if is_prime(num):
        sum_of_primes += num
    num += 2

print(sum_of_primes)
