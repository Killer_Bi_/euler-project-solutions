# 2520 - самое маленькое число, которое делится без остатка на все числа от 1 до 10.
#
# Какое самое маленькое число делится нацело на все числа от 1 до 20?


i = 380
list_of_remainders = []

while True:
    list_of_remainders = [i % num for num in range(1,21)]
    if sum(list_of_remainders) == 0:
        break
    i += 380

print(i)
