# Последовательность Фибоначчи определяется рекурсивным правилом:
#
# Fn = Fn−1 + Fn−2, где F1 = 1 и F2 = 1.
#
# Каков порядковый номер первого члена последовательности Фибоначчи, содержащего 1000 цифр?


fib_list = [1, 1]
index = 2

while True:
    fib_list[0], fib_list[1] = fib_list[1], fib_list[0] + fib_list[1]
    index += 1
    if len(str(fib_list[1])) == 1000:
        break

print(index)
