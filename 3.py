# # Простые делители числа 13195 - это 5, 7, 13 и 29.
# #
# # Каков самый большой делитель числа 600851475143,
# # являющийся простым числом?
#
#
# def is_prime(n):
#     if n % 2 == 0:
#         return n == 2
#     for i in range(3, int(n**(1/2)) + 1, 2):
#         if n % i == 0:
#             return False
#     return True
#
#
# def greatest_prime_divisor(num):
#     for i in range(num//2 + 1, 1, -1):
#         if num % i == 0:
#             if is_prime(i):
#                 return i
#
#
# number = 600851475143
# print(greatest_prime_divisor(number))
from math import sqrt

ans = 2
n = 600851475143

while (n%2 == 0):
    n /= 2

i = 3
while n > 1:
    while (n%i == 0):
        ans = i
        n /= i
    i += 2
print(ans)