# Если записать числа от 1 до 5 английскими словами (one, two, three, four, five),
# то используется всего 3 + 3 + 5 + 4 + 4 = 19 букв.
#
# Сколько букв понадобится для записи всех чисел от 1 до 1000 (one thousand) включительно?
#
#
# Примечание: Не считайте пробелы и дефисы.
# Например, число 342 (three hundred and forty-two) состоит из 23 букв,
# число 115 (one hundred and fifteen) - из 20 букв.
# Использование "and" при записи чисел соответствует правилам британского английского.


counter = 0
fir_num = 0
sec_num = 0
thr_num = 0
div_100 = 0

for i in range(1, 1001):
    fir_num = i % 10
    div_100 = i % 100
    if div_100 == 11 or div_100 == 12:
        counter += 6
    elif div_100 == 13 or div_100 == 14 or div_100 == 18 or div_100 == 19:
        counter += 8
    elif div_100 == 15 or div_100 == 16:
        counter += 7
    elif div_100 == 17:
        counter += 9
    elif fir_num == 1 or fir_num == 2 or fir_num == 6 or div_100 == 10:
        counter += 3
    elif fir_num == 3 or fir_num == 8 or fir_num == 7:
        counter += 5
    elif fir_num == 4 or fir_num == 5 or fir_num == 9:
        counter += 4

    if len(str(i)) >= 2:
        sec_num = int(str(i)[-2])
        if sec_num == 2 or sec_num == 3 or sec_num == 8 or sec_num == 9:
            counter += 6
        elif sec_num == 4 or sec_num == 5 or sec_num == 6:
            counter += 5
        elif sec_num == 7:
            counter += 7

        if len(str(i)) >= 3:
            thr_num = int(str(i)[-3])
            if div_100 != 0:
                counter += 3
            if thr_num == 1 or thr_num == 2 or thr_num == 6:
                counter += 10
            elif thr_num == 3 or thr_num == 8 or thr_num == 7:
                counter += 12
            elif thr_num == 4 or thr_num == 5 or thr_num == 9:
                counter += 11

    if i == 1000:
        counter += 11

print(counter)
