# Пусть d(n) определяется как сумма делителей n (числа меньше n, делящие n нацело).
# Если d(a) = b и d(b) = a, где a ≠ b, то a и b называются дружественной парой,
# а каждое из чисел a и b - дружественным числом.
#
# Например, делителями числа 220 являются 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 и 110,
# поэтому d(220) = 284. Делители 284 - 1, 2, 4, 71, 142, поэтому d(284) = 220.
#
# Подсчитайте сумму всех дружественных чисел меньше 10000.


def sum_of_div(num):
    sum_div = 0
    for i in range(1, num):
        if num % i == 0:
            sum_div += i
    return sum_div


list_of_friendly_nums = []
sum_of_friendly_nums = 0

for i in range(10000):
    sum_i = sum_of_div(i)
    if i == sum_of_div(sum_i) and i != sum_i and sum_i not in list_of_friendly_nums:
        sum_of_friendly_nums += i + sum_i
        list_of_friendly_nums.append(i)

print(sum_of_friendly_nums)
