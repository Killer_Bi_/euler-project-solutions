# Начиная с числа 1 и двигаясь дальше вправо по часовой стрелке,
# образуется следующая спираль 5 на 5:
#
# 21 22 23 24 25
# 20  7  8  9 10
# 19  6  1  2 11
# 18  5  4  3 12
# 17 16 15 14 13
#
# Можно убедиться, что сумма чисел в диагоналях равна 101.
#
# Какова сумма чисел в диагоналях спирали 1001 на 1001,
# образованной таким же способом?


n = 1001
matrix = []

for i in range(n):
    matrix.append([])
    for j in range(n):
        matrix[i].append(0)

counter = 1
i = j = int((n - 1) / 2)
matrix[i][j] = counter
j += 1
sum_of_nums = 0
last_stage = False
k1 = 0
k2 = n - 1

while True:
    counter += 1
    if j - 1 >= 0 and not last_stage:
        if matrix[i][j - 1] != 0 and matrix[i + 1][j] == 0:
            matrix[i][j] = counter
            i += 1
        elif matrix[i - 1][j] != 0:
            matrix[i][j] = counter
            j -= 1
        elif matrix[i][j + 1] != 0:
            matrix[i][j] = counter
            i -= 1
        elif matrix[i + 1][j] != 0:
            matrix[i][j] = counter
            j += 1
    else:
        last_stage = True
    if last_stage:
        if matrix[i][j + 1] != 0:
            matrix[i][j] = counter
            i -= 1
        elif j + 2 < n:
            if matrix[i + 1][j] != 0:
                matrix[i][j] = counter
                j += 1
        else:
            matrix[i][j] = counter
            matrix[i][j + 1] = counter + 1
            break

for i in range(n):
    for j in range(n):
        if i == j and matrix[i][j] != 1:
            sum_of_nums += matrix[i][j]
        elif i == k1 and j == k2:
            sum_of_nums += matrix[i][j]
            k1 += 1
            k2 -= 1
print(sum_of_nums)