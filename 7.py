# Выписав первые шесть простых чисел, получим 2, 3, 5, 7, 11 и 13.
# Очевидно, что 6-е простое число - 13.
#
# Какое число является 10001-м простым числом?


def is_prime(n):
    if n % 2 == 0:
        return n == 2
    for i in range(3, int(n**(1/2)) + 1, 2):
        if n % i == 0:
            return False
    return True


counter = 0
num = 1
while True:
    if is_prime(num):
        if counter == 10001:
            break
        counter += 1
    num += 1

print(num)
